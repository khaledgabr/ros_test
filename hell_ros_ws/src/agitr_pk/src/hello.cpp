#include <ros/ros.h>

int main(int argc, char **argv) {
    ros::init(argc, argv, "hello_ros");

    ros::NodeHandle nh;
    ros::Rate rrate(5);
    while (ros::ok()) {
        ROS_INFO_STREAM("Hell, ROS!");
        rrate.sleep();
    }
}
